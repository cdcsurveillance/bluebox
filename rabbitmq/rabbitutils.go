package rabbitmq

import (
  "log"
  "github.com/streadway/amqp"
)

func GetQueue(name string) *amqp.Queue {

  q, err := ch.QueueDeclare(
    name, // name
    false,   // durable
    false,   // delete when unused
    false,   // exclusive
    false,   // no-wait
    nil,     // arguments
  )
  if err != nil { log.Fatalf("Failed to declare %s queue, error: %s", name, err)
  } else { log.Printf("Declared queue: %s", name) }

  return &q
}// ./GetQueue


func SendMessage(q *amqp.Queue, msg []byte) {

  err := ch.Publish(
    "",     // exchange
    q.Name, // routing key
    false,  // mandatory
    false,  // immediate
    amqp.Publishing{
      ContentType: "application/json",
      Body:        msg,
    })

  if err != nil { log.Fatalf("%s: %s", string(msg), err)
  } else { log.Printf(" [x] Sent %s", string(msg)) }

}// ./sendMessage


