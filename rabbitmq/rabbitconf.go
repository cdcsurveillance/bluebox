package rabbitmq

import (
  "log"
  "github.com/streadway/amqp"
  "time"
)

var conn *amqp.Connection
var ch *amqp.Channel 

const UploadedMessageQueue = "UploadedMessageQueue"
const DeletedMessageQueue = "DeletedMessageQueue"

var err error

func GetChannel(url string)  (*amqp.Connection, *amqp.Channel) {

  for {

    conn, err = amqp.Dial(url)

    if err == nil {   
      log.Println("Connected to RabbitMQ")
      break
    }// .err == nil

    time.Sleep(1250 * time.Millisecond)
  }// .for

  for {

    ch, err = conn.Channel()

    if err == nil { 
      log.Println("Connected to RabbitMQ Channel")
      break
    }// .err == nil
   
    time.Sleep(1000 * time.Millisecond)
  }// .for

  return conn, ch
}// ./GetChannel



