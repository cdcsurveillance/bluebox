# bluebox service
FROM golang:1.10.3-alpine3.8

MAINTAINER lrp4

RUN apk update && apk upgrade && apk add --no-cache bash git
RUN go get github.com/globalsign/mgo
RUN go get github.com/minio/minio-go
RUN go get github.com/streadway/amqp

ENV SOURCES /bluebox/
COPY . ${SOURCES}

RUN cd ${SOURCES} && CGO_ENABLED=0  go build
WORKDIR ${SOURCES}

ENV BLUEBOX_PORT 4001
EXPOSE 4001

CMD sleep 10; ${SOURCES}bluebox

# to build:
# docker build -t esurveillance/bluebox .
