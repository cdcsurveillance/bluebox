package api

import (
  "net/http"
  "log"
  "../updfile"
  "io"
)

/* ---------------------------------------------------
    /{grantee}/submissions/{hashid}{/content}
-----------------------------------------------------*/
func handleGranteeHashContent(w http.ResponseWriter, r *http.Request, grantee string, hash string) {
  
  //log.Printf("handleGranteeHashContent, grantee: %s hash: %s", grantee, hash)
  /* -------------------------------------
                   GET 
  -------------------------------------- */
  if r.Method == http.MethodGet {

    // reading a file from minio
    file, err := updfile.FromMinio(grantee, hash)
      
    if err != nil {
      log.Println("Error getting file from minio", err)

      w.WriteHeader(http.StatusInternalServerError)
      w.Write([]byte("Error getting message file from minio"))
      return
    }

    // streaming the file
    if _, err = io.Copy(w, file); err != nil {
      
      log.Println("Error streaming file", err)

      w.WriteHeader(http.StatusInternalServerError)
      w.Write([]byte("Error streaming message file"))
      return
    } 

    return
  }// ./r.Method == http.MethodGet


  w.WriteHeader(http.StatusMethodNotAllowed)
  w.Write([]byte("method not supported"))
  return
} // ./handleGranteeHash

