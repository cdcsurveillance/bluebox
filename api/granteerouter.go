package api

import (
  "net/http"
  "regexp"
  "log"
  "../validuser"
)

/* -------------------------------------
TODO: pilot only, convert to login middleware
---------------------------------------*/
func CheckGranteeValid(grantee string) bool {

  err, pilotTempUsers := validuser.GetValidPilotUsers()
  if (err!=nil) {
    log.Println(err)
  }
  log.Println("CheckGranteeValid, GetValidPilotUsers:", pilotTempUsers)

  // var pilotTempUsers = [9]string{"CDC", "10", "12", "13", "16", "17", "18", "19", "20"}

  for _, user := range pilotTempUsers {
    if grantee == user { return true }
  }

  return false
}// ./CheckGranteeValid


/* ---------------------------------------------------
  All Routes: {grantee}/submissions/{hashid}{/content}
-----------------------------------------------------*/
func GranteeRouter(w http.ResponseWriter, r *http.Request) {

  //TODO:
  w.Header().Add("Access-Control-Allow-Origin", "*")


  /* ---------------------------------------------------
           /{grantee}/submissions
  -----------------------------------------------------*/
  pattern1, _ := regexp.Compile(`\/(.*)\/submissions\z`)
  pattern1Arr := pattern1.FindStringSubmatch(r.URL.Path)

  if len(pattern1Arr) > 0 {
    grantee := pattern1Arr[1]
    //log.Println("match {grantee}/submissions", grantee)

    if CheckGranteeValid(grantee) {
      handleGrantee(w, r, grantee)
      return
    } else {
      w.WriteHeader(http.StatusForbidden)
      w.Write([]byte("request/grantee not valid"))
      return
    }
  }// ./if len(pattern1Arr) > 0


  /* ---------------------------------------------------
       /{grantee}/submissions/{hashid}{/content}
  -----------------------------------------------------*/
  pattern3, _ := regexp.Compile(`\/(.*)\/(submissions)\/(.*)\/(content)`)
  pattern3Arr := pattern3.FindStringSubmatch(r.URL.Path)

  if len(pattern3Arr) > 0 {
    grantee := pattern3Arr[1]
    hash := pattern3Arr[3]
    //log.Println("match {grantee}/submissions/{hash}/content", grantee, hash)

    if CheckGranteeValid(grantee) {
      handleGranteeHashContent(w, r, grantee, hash)
      return
    } else {
      w.WriteHeader(http.StatusForbidden)
      w.Write([]byte("request/grantee not valid"))
      return
    }
  }// ./if len(pattern3Arr) > 0


  /* ---------------------------------------------------
       /{grantee}/submissions/{hashid}
  -----------------------------------------------------*/
  pattern2, _ := regexp.Compile(`\/(.*)\/(submissions)\/(.*)`)
  pattern2Arr := pattern2.FindStringSubmatch(r.URL.Path)

  if len(pattern2Arr) > 0 {
    grantee := pattern2Arr[1]
    hash := pattern2Arr[3]
    //log.Println("match {grantee}/submissions/{hash}", grantee, hash)

    if CheckGranteeValid(grantee) {
      handleGranteeHash(w, r, grantee, hash)
      return
    } else {
      w.WriteHeader(http.StatusForbidden)
      w.Write([]byte("request/grantee not valid"))
      return
    }
  }// ./if len(pattern2Arr) > 0


  /* ---------------------------------------------------
       /...remaining routes
  -----------------------------------------------------*/
  w.WriteHeader(http.StatusBadRequest)
  w.Write([]byte("route not valid"))
  return

} // ./GranteeSubmissions





