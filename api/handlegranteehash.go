package api

import (
  "net/http"
  "log"
  "encoding/json"
  "../updmetadata"
  "../updfile"
  "../rabbitmq"
)

/* ---------------------------------------------------
     /{grantee}/submissions/{hashid}
-----------------------------------------------------*/
func handleGranteeHash(w http.ResponseWriter, r *http.Request, grantee string, hash string) {
  
  //log.Printf("handleGranteeHash, grantee: %s hash: %s", grantee, hash)

  /* -------------------------------------
                   GET 
  -------------------------------------- */
  if r.Method == http.MethodGet {
    getMetaDataByHash(w, r, hash) 
    return
  }// ./r.Method == http.MethodGet

  /* -------------------------------------
                   DELETE 
  -------------------------------------- */
  if r.Method == http.MethodDelete {
    deleteSubmissionByHash(w, r, grantee, hash)
    return
  }// ./r.Method == http.MethodDelete

  w.WriteHeader(http.StatusMethodNotAllowed)
  w.Write([]byte("method not supported"))
  return

} // ./handleGranteeHash


/* -------------------------------------
                 GET 
-------------------------------------- */
func getMetaDataByHash(w http.ResponseWriter, r *http.Request, hash string) {

  log.Printf("Get meta data for hash: %s", hash)

  umd, err := updmetadata.FromMongoByHash(hash)
    
  if err!=nil {
    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte("metadata not available"))
    return
  }// ./err!=nil

  w.WriteHeader(http.StatusOK)
  json.NewEncoder(w).Encode(umd)
  return
}// ./getMetaDataByHash


/* -------------------------------------
                 DELETE 
-------------------------------------- */
func deleteSubmissionByHash(w http.ResponseWriter, r *http.Request, grantee string, hash string) {

  log.Printf("Delete meta data + file, for hash: %s", hash)

  // remove metadata
  updMetaData := updmetadata.UpdMetaData{}
  updMetaData.UpdHashFileName = hash

  err1 := updMetaData.RemoveFromMongo()

  if err1 != nil { 
    log.Println("Error remove from Mongo: ", err1) 
    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte("unable to remove metadata"))
    return

  } else {
    // ./metadata removed success

    // remove message/file
    err2 := updfile.RemoveFromMinio(grantee, hash)

    if err2 != nil { 
      log.Println("Error remove from Minio: ", err2) 
      w.WriteHeader(http.StatusInternalServerError)
      w.Write([]byte("unable to remove message file"))
      return

    } else {
      // both metadata and message file removed success
      log.Println("Removed metadata and message/file success") 

      // delete hash message to rabbit
      q := rabbitmq.GetQueue(rabbitmq.DeletedMessageQueue)
      message, _ := updMetaData.ToJSON()
      rabbitmq.SendMessage(q, message)

      // response
      w.WriteHeader(http.StatusOK)
      w.Write([]byte("metadata and message/file removed"))
      return
    }// ./else

  }// ./else
    
}// ./deleteSubmissionByHash


