package api

import (
  "net/http"
  //"log"
  "encoding/json"
  "../updmetadata"
  "time"
  "crypto/sha1"
  "strconv"
  "encoding/hex"
  "../updfile"
  "../rabbitmq"
  //"../statefips"
)

/* ---------------------------------------------------
           /{grantee}/submissions
-----------------------------------------------------*/
func handleGrantee(w http.ResponseWriter, r *http.Request, grantee string) {

  //log.Println("handleGrantee:", grantee)

  /* -------------------------------------
                   POST
  -------------------------------------- */
  if r.Method == http.MethodPost {
    granteeUpload(w, r, grantee)
    return
  }// ./r.Method == http.MethodPost

  /* -------------------------------------
                   GET
  -------------------------------------- */
  if r.Method == http.MethodGet {
    granteeSubmissions(w, r, grantee)
    return
  }// ./r.Method == http.MethodGet

  w.WriteHeader(http.StatusMethodNotAllowed)
  w.Write([]byte("method not supported"))
  return
} // ./handleGrantee


/* -------------------------------------
                 GET
-------------------------------------- */
func granteeSubmissions(w http.ResponseWriter, r *http.Request, grantee string) {

  umds, err := updmetadata.FromMongoGrantee(grantee)

  if err!=nil {
    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte("metadata not available"))
    return
  }// ./err!=nil

  w.WriteHeader(http.StatusOK)
  json.NewEncoder(w).Encode(umds)
  return
}// ./granteeUpload

type HashName struct {
  Hash string `json:"hash"`
}// ./HashName
/* -------------------------------------
                 POST
-------------------------------------- */
func granteeUpload(w http.ResponseWriter, r *http.Request, grantee string) {

  //parse of incoming request form
  r.ParseMultipartForm(32 << 20)

  //get values out of request
  //grantee := r.FormValue("grantee"), not needed, in route

  updStateFips := r.FormValue("updstatefips")
  updEmail := r.FormValue("updemail")
  updClientTime := r.FormValue("updclienttime")
  updFileName := r.FormValue("updfilename")


  //check values non empty
  if len(updEmail) == 0 || len(updClientTime) == 0 {
    w.WriteHeader(http.StatusBadRequest)
    w.Write([]byte("metadata not valid"))
    return
  }// ./check values non empty

  //get and check the uploaded file
  file, fileHdr, err := r.FormFile("updfile")
  if err != nil { // not able to receive file
    w.WriteHeader(http.StatusBadRequest)
    w.Write([]byte("file not valid"))
    return
  }// ./get and check the uploaded file

  //close the file
  defer file.Close()

  //original file name, change to receive from client for IE issue
  //updFileName := fileHdr.Filename

  //adding server time and hash to metadata
  serverTimeStr := strconv.FormatInt( time.Now().UnixNano() / int64(time.Millisecond) , 10 )

  //using hash for unique filename
  hashStr := grantee + updEmail + updClientTime + updFileName + serverTimeStr
  sha_1 := sha1.New()
  sha_1.Write([]byte(hashStr))

  hash := hex.EncodeToString(sha_1.Sum(nil))

  //uploaded file metadata
  // Grantee, UpdEmail, UpdClientTime, UpdFileName, UpdServerTime, UpdHashFileName
  updMetaData := updmetadata.UpdMetaData{grantee, updEmail, updClientTime, updFileName, serverTimeStr, hash, updStateFips}
  //fmt.Println(updMetaData)

  /* -------------------------------------
  //saving uploaded file metadata to Mongo
  -------------------------------------- */
  err = updMetaData.ToMongo()
  if err!=nil {
    w.WriteHeader(http.StatusInternalServerError)
    w.Write([]byte("metadata not saved"))
    return

  } else {
    // ./saved to Mongo successful, need to save to Minio also

    /* -------------------------------------
    //saving uploaded file to Minio, using hash as file name
    -------------------------------------- */

    err = updfile.ToMinio(grantee, hash, fileHdr)
    if err!=nil {
      w.WriteHeader(http.StatusInternalServerError)
      w.Write([]byte("file not saved"))

      //Minio error, need to delete orphan Mongo data
      // TODO: handle this edge case concurently, Mongo, Minio, when only one is up and one is down
      // right now there may be a few seconds where Mongo and Minio data is not in sync, for this edge case
      _ = updMetaData.RemoveFromMongo()

      return
    }
    // ./saved successful to Minio

    /* -------------------------------------
         notification to rabbitMQ
    -------------------------------------- */
    q := rabbitmq.GetQueue(rabbitmq.UploadedMessageQueue)
    message, _ := updMetaData.ToJSON()
    rabbitmq.SendMessage(q, message)

    /* -------------------------------------
    // returning response OK
    -------------------------------------- */
    w.WriteHeader(http.StatusOK)
    //w.Write([]byte("File uploaded success"))
    json.NewEncoder(w).Encode(HashName{Hash:hash})
    return
  }// ./saved to Mongo & Minio successful

}// ./granteeUpload

