package api

import (
  "encoding/json"
  "net/http"
  "../updmetadata"
)

/* -------------------------------------
          Route /submissions
 ---------------------------------------*/
func Submissions(w http.ResponseWriter, r *http.Request) {

  //TODO:
  w.Header().Add("Access-Control-Allow-Origin", "*")

  // TODO: from login service: check if grantee is valid CDC user role

  if r.Method == http.MethodGet {

    var umds []updmetadata.UpdMetaData
    var err error

    umds, err = updmetadata.FromMongoAll() 

    if err!=nil {
      w.WriteHeader(http.StatusInternalServerError)
      w.Write([]byte("metadata not available"))
      return
    }// ./err!=nil

    w.WriteHeader(http.StatusOK)
    json.NewEncoder(w).Encode(umds)
    return

  } // ./r.Method == http.MethodGet

  // Method is not GET, not supported
  w.WriteHeader(http.StatusMethodNotAllowed)
  w.Write([]byte("method not supported"))
  return

}// ./Submissions

