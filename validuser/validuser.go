package validuser

import (
  "net/http"
  "io/ioutil"
  "log"
  "encoding/json"
)

type pilotUser struct {
    User   string `json:"user"`
    UserId   string `json:"userid"`
    State     string  `json:"state"`
    Fips      string     `json:"fips"`
}// .PilotUser

func GetValidPilotUsers() (error, []string){

  pilotUsersIds := []string{}
  var pilotUsersArr []pilotUser

  wwuserUrl := "http://wwuser:7070/users"
  resp, err := http.Get(wwuserUrl)

  if (err != nil) {
    log.Println("Error getting User Table from wwuser service")
    return err, pilotUsersIds
  } else {
    userTable, _ := ioutil.ReadAll(resp.Body)
    // log.Println(string(userTable))

    err := json.Unmarshal(userTable, &pilotUsersArr)
    if (err!=nil) {
      log.Println("Err Unmarshal", err)
    }

    for _, user := range pilotUsersArr {
      pilotUsersIds = append(pilotUsersIds, user.UserId)
    }

    return nil, pilotUsersIds
  } // if

}// .GetValidPilotUsers
