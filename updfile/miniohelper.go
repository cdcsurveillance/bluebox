package updfile

import (
  //"log"  
  "mime/multipart"
  "github.com/minio/minio-go"
)


/* -------------------------------------
        stores files to Minio
---------------------------------------*/
func ToMinio(grantee string, fileName string, fileHdr *multipart.FileHeader)  error {

  // TODO: set bucket and location from grantee name
  bucket := BucketName
  bucketLocation := BucketLocation

  //create a bucket if it does not exist
  err := MinioClient.MakeBucket(bucket, bucketLocation)
  if err != nil {

    //check if already own this bucket
    exists, err := MinioClient.BucketExists(bucket)
    if err == nil && exists {
      //log.Printf("We already own %s\n", bucket)
    } else {
      return err 
    }

  }// ./err != nil
  //log.Printf("Successfully created %s\n", bucket)

  //opent file to save
  handler, err := fileHdr.Open()
  if err!=nil { return(err) }

  // Upload the file
  // n, err := MinioClient.PutObject(bucket, fileName, handler, fileHdr.Size, minio.PutObjectOptions{ContentType:"text/plain"}) 
  _, err = MinioClient.PutObject(bucket, fileName, handler, fileHdr.Size, minio.PutObjectOptions{ContentType:"text/plain"}) 

  if err != nil { return(err) }
  
  //log.Printf("Successfully uploaded %s of size %d\n", fileName, n)
  return nil
}// ./


/* -------------------------------------
      removes a file from Minio
---------------------------------------*/
func RemoveFromMinio(grantee string, fileName string) error {
  
  // TODO: set bucket and location from grantee name
  bucket := BucketName

  err := MinioClient.RemoveObject(bucket, fileName)
  
  if err != nil { return err }

  return nil
}// ./RemoveFromMinio


/* -------------------------------------
      retrieves a file from Minio
---------------------------------------*/
func FromMinio(grantee string, fileName string) ( *minio.Object, error) {

  // TODO: set bucket and location from grantee name
  bucket := BucketName

  fileObj, err := MinioClient.GetObject(bucket, fileName, minio.GetObjectOptions{})

  if err != nil { return nil, err }

  return fileObj, nil
}// ./GetFromMinio




