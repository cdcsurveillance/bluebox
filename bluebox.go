package main

import (
  "./portconfig"
  "log"
  "fmt"
	"net/http"
  "./api"
  "./rabbitmq"
  "./updmetadata"
  "os"
)


func main() {

  /* -------------------------------------
        Mongo session
  ---------------------------------------*/
  urlmongo := os.Getenv("MONGO_SERVICE_NAME") + portconfig.PortMongo() // docker: mgo.Dial("mongo:27017") // local: mgo.Dial("mongodb://localhost/uploads")
  mgoSession := updmetadata.GetMgoSession(urlmongo)
  defer mgoSession.Close()

  /* -------------------------------------
        RabbitMQ connection
  ---------------------------------------*/
  urlrabbit := "amqp://" + os.Getenv("RABBIT_PASSWORD") + ":" + os.Getenv("RABBIT_USERNAME") + "@rabbitmq" + portconfig.PortRabbitMQ()
  conn, ch := rabbitmq.GetChannel(urlrabbit)
  defer conn.Close()
  defer ch.Close()
  

  /* -------------------------------------
            Routes handling
  ---------------------------------------*/

  //serves all metadata, for all messages, CDC role user only
  http.HandleFunc("/submissions", api.Submissions)

  //grantee router
	http.HandleFunc("/", api.GranteeRouter)

  http.Handle("/favicon.ico", http.NotFoundHandler())

  /* -------------------------------------
        Port config from env var
  ---------------------------------------*/
  port := portconfig.PortBluebox()
  /* -------------------------------------
        Start server if ok
  ---------------------------------------*/
  fmt.Println("Listening " + port)
	log.Fatalln( http.ListenAndServe(port, nil) )

}// ./main


