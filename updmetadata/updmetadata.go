package updmetadata

import (
  "encoding/json"
  "github.com/globalsign/mgo/bson"
)

/* -------------------------------------
          metadata type
---------------------------------------*/
type UpdMetaData struct {
  //from user
  Grantee string  `json:"grantee"`
  UpdEmail string `json:"updemail"`
  UpdClientTime string `json:"updclienttime"`
  UpdFileName string `json:"updfilename"`
  //from server
  UpdServerTime string `json:"updservertime"`
  UpdHashFileName string `json:"updhashfilename"`
  // from user service
  UpdStateFIPS string `json:"updstatefips"`
}// ./UpdMetaData

// retrieves  metadata from Mongo for grantee by hash
func FromMongoByHash(hash string) (UpdMetaData, error) {
  umd := UpdMetaData{}
  err := MetaData.Find(bson.M{"updhashfilename": hash}).One(&umd)
  if err != nil {
    return umd, err
  }
  return umd, nil
}// ./FromMongoGrantee

// retrieves all the metadata from Mongo for a given grantee
func FromMongoGrantee(grantee string) ([]UpdMetaData, error) {
  umds := []UpdMetaData{}
  err := MetaData.Find(bson.M{"grantee": grantee}).All(&umds)
  if err != nil {
    return nil, err
  }
  return umds, nil
}// ./FromMongoGrantee


// retrieves all metadata from  Mongo
func FromMongoAll() ([]UpdMetaData, error) {
  umds := []UpdMetaData{}
  err := MetaData.Find(bson.M{}).All(&umds)
  if err != nil {
    return nil, err
  }
  return umds, nil
}// ./fromMongoAll


// saves UpdMetaData to Mongo
func (umd UpdMetaData) ToMongo() error {

  err := MetaData.Insert(umd)

  if err != nil { return err }

  return nil
}// ./ToMongo

// removes UpdMetaData from Mongo
func (umd UpdMetaData) RemoveFromMongo() error {

  err := MetaData.Remove(bson.M{"updhashfilename": umd.UpdHashFileName})

  if err != nil { return err }

  return nil
}// ./DeleteFromMongo


// UpdMetaData converted to JSON
func (u UpdMetaData) ToJSON() ([]byte, error) {

  umd, err := json.Marshal(u)

  if err!=nil { return []byte(""), err }

  return umd, nil
}// ./ToJSON


// JSON converted to UpdMetaData
func (u UpdMetaData) FromJSON(data []byte) (UpdMetaData, error) {

  umd := UpdMetaData{}
  err := json.Unmarshal(data, &umd)

  if err!=nil {
    return UpdMetaData{}, err
  }

  return umd, nil
}// ./FromJSON

