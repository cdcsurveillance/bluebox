package updmetadata

import (
  "fmt"
  "github.com/globalsign/mgo"
)

//session
var s *mgo.Session

//database
var DB *mgo.Database

// collection
var MetaData *mgo.Collection


func GetMgoSession(url string) *mgo.Session {
  var err error

  //get a mongo sessions
  s, err = mgo.Dial(url) 

  if err != nil { 
    fmt.Println("Mongo Panic")
    panic(err) 
  }

  if err = s.Ping(); err != nil {
    fmt.Println("Mongo Ping Panic") 
    panic(err) 
  }

  DB = s.DB("uploads")
  MetaData = DB.C("metadata")

  fmt.Println("Connected to Mongo")

  return s
}// ./GetMongoSession


