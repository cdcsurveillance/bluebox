package portconfig

import (
  "os"
  "log"
  )

func PortBluebox() string {

  port := os.Getenv("BLUEBOX_PORT")

  if len(port) == 0 {
    log.Fatal("Missing BLUEBOX_PORT env var")
  }

  //log.Println("BLUEBOX_PORT:", port)
  return ":" + port
}// ./PortBluebox

func PortMongo() string {

  port := os.Getenv("MONGO_PORT")

  if len(port) == 0 {
    log.Fatal("Missing MONGO_PORT env var")
  }

  //log.Println("MONGO_PORT:", port)
  return ":" + port
}// ./PortMongo

func PortMinio() string {

  port := os.Getenv("MINIO_PORT")

  if len(port) == 0 {
    log.Fatal("Missing MINIO_PORT env var")
  }

  //log.Println("MINIO_PORT:", port)
  return ":" + port
}// ./PortMongo

func PortRabbitMQ() string {

  port := os.Getenv("RABBIT_PORT")

  if len(port) == 0 {
    log.Fatal("Missing RABBIT_PORT env var")
  }

  //log.Println("RABBIT_PORT:", port)
  return ":" + port
}// ./PortRabbitMQ






